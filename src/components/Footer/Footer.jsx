import React from "react";
import './Footer.css'
import './Icons.css'
import { FaInstagramSquare } from "react-icons/fa";
import { FaTelegram } from "react-icons/fa";
import { FaGooglePlusSquare } from "react-icons/fa";





const Footer =() => {
    return (
        <footer className="Footer">

          <ul>
            <li>
              <a href="#" >
              <span className="Icons">
                <FaInstagramSquare />
              </span>
              </a>
            </li>
              
            <li>
              <a href="#" >
                <span className="Icons">
                  <FaTelegram />
                </span>
                </a>
            </li>

            <li>
              <a href="#" >
              <span className="Icons">
                <FaGooglePlusSquare />
              </span>
              </a>
            </li>
          </ul>

          <span className="Reserved">@ 2022. All rights Reserved.</span>
          

        </footer>
    )
}

export default Footer;